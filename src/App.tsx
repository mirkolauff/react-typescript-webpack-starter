import './styles.css'
import Logo from './logo.png'
import LogoSvg from './logo.svg'
import { ClickCounter } from './ClickCounter'

export const App = () => {
  return (
    <>
      <h1>
        React Typescript Webpack Starter Template - {process.env.NODE_ENV}{' '}
        {process.env.name}
      </h1>
      <img src={Logo} alt="React Logo" width="300" height="300" />
      <img src={LogoSvg} alt="React Logo" width="300" />
      <ClickCounter />
    </>
  )
}
